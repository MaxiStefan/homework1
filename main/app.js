function distance(first, second){
	if( first instanceof Array &&  second instanceof Array)
	{
		if(first.length === null || second.length === null)
				return 0;
			else {
				diff1 = first.filter(x => !second.includes(x)) 
				diff2 = second.filter( x => !first.includes(x))
				let newArray= diff1.concat(diff2)
				let refinedArray = [... new Set(newArray)]

			return refinedArray.length;
		}
	}
	else throw  new Error('InvalidType');
}


module.exports.distance = distance